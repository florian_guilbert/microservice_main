from flask import Flask, jsonify, json, abort
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from sqlalchemy import UniqueConstraint

import requests

from dataclasses import dataclass
from producer import publish

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql://root:root@db/main'

CORS(app)
db = SQLAlchemy(app)

@dataclass
class Product(db.Model):
    id: int
    title: str
    image: str
    likes: int

    id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    title = db.Column(db.String(200))
    image = db.Column(db.String(200))
    likes = db.Column(db.Integer)

@dataclass
class ProductUser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    product_id = db.Column(db.Integer)
    __table_args__ = (db.UniqueConstraint('user_id', 'product_id'), )


@app.route('/api/products')
def index():
    return jsonify(Product.query.all())

@app.route('/api/products/<int:id>/like', methods=['POST'])
def like(id):
    req = requests.get('http://backendadmin:8000/api/user')
    json = req.json()

    try:
        productUser = ProductUser(user_id=json['id'], product_id=id)
        db.session.add(productUser)
        db.session.commit()
        publish('product_liked', id)
        print("product {0} liked by {1}".format(id, json['id']))
        return jsonify({
            'message': 'success',
            'user': json['id']
        })
    except Exception as e:
        #meaning if same user wants to like again same product
        print('error - {0}'.format(e))
        return jsonify({
            'message': 'fail_already_likes',
            'user': json['id']
        })
        #abort(400, 'You already liked this product - {0}'.format(e))


    try:
        product = Product.query.get(id)
        print(product)
        product.likes += 1
        db.session.commit()
    except Exception as e:
        print('error main - {0}'.format(e))
        abort(500, 'Pb with main db - {0}'.format(e))

    return jsonify({
        'message': 'success'
    })

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
