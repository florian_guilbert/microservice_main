# amqps://luntpnxt:X2wuIZ6WJoiSgQ3mpvFWtyHb3GS4yfca@rattlesnake.rmq.cloudamqp.com/luntpnxt
import pika, json

params = pika.URLParameters("amqps://luntpnxt:X2wuIZ6WJoiSgQ3mpvFWtyHb3GS4yfca@rattlesnake.rmq.cloudamqp.com/luntpnxt")
connection = pika.BlockingConnection(params)
channel = connection.channel()

def publish(method, body):
    properties = pika.BasicProperties(method)
    retry = 0
    while 1:
        try:
            channel.basic_publish(exchange='', routing_key='admin', body=json.dumps(body), properties=properties)
            print('published')
            break
        except Exception as e:
            print("pb try {0}/5 type {1}".format(retry, type(e)))
            retry +=1
            if retry > 5:
                raise e
                break
            params = pika.URLParameters("amqps://luntpnxt:X2wuIZ6WJoiSgQ3mpvFWtyHb3GS4yfca@rattlesnake.rmq.cloudamqp.com/luntpnxt")
            connection = pika.BlockingConnection(params)
            channel = connection.channel()
