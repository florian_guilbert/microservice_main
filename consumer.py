# amqps://luntpnxt:X2wuIZ6WJoiSgQ3mpvFWtyHb3GS4yfca@rattlesnake.rmq.cloudamqp.com/luntpnxt
import pika, json
from main import Product, db

params = pika.URLParameters("amqps://luntpnxt:X2wuIZ6WJoiSgQ3mpvFWtyHb3GS4yfca@rattlesnake.rmq.cloudamqp.com/luntpnxt")
connection = pika.BlockingConnection(params)
channel = connection.channel()

channel.queue_declare(queue='main')

def callback(ch, method, properties, body):
    print("Received in main content type {0}".format(properties.content_type))
    data = json.loads(body)
    print(data)

    if properties.content_type == 'product_created':
        try:
            product = Product(id=data['id'], title=data['title'], image=data['image'], likes=0)
            print(product)
            db.session.add(product)
            db.session.commit()
            print("product created")
        except Exception as e:
            print("product already created error {0}".format(e))

    elif properties.content_type == 'product_updated':
        try:
            product = Product.query.get(data['id'])
            product.title = data['title']
            product.image = data['image']
            db.session.commit()
            print("product updated")
        except Exception as e:
            print('error update {0}'.format(e))

    elif properties.content_type == 'product_deleted':
        try:
            product = Product.query.get(data) # data is the id not in dict
            db.session.delete(product)
            db.session.commit()
            print("product deleted")
        except Exception as e:
            print("error delete".format(e))

channel.basic_consume(queue='main', on_message_callback=callback, auto_ack=True)

print("started consuming")

channel.start_consuming()

channel.close()
